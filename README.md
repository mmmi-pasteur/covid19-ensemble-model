# COVID19-Ensemble-Model

Code and data for the article by Paireau et al. "An ensemble model based on early predictors to forecast COVID-19 healthcare demand in France".
The script Main.R allows one to run the analyses and plot the results.
